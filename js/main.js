
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, onValue, ref as refS, set, child, get, update, remove }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAcd4nmM-bSh8l3mfxZO17Kh1gknIbZANI",
    authDomain: "proyectowebmundo-81107.firebaseapp.com",
    databaseURL: "https://proyectowebmundo-81107-default-rtdb.firebaseio.com",
    projectId: "proyectowebmundo-81107",
    storageBucket: "proyectowebmundo-81107.appspot.com",
    messagingSenderId: "600676758823",
    appId: "1:600676758823:web:1dcb27e878bb55d3f2a502"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

//declarar unas Variables global
var numCodigo = 0;
var categoria = "";
var nombreL = "";
var precio = 0;
var urlImag = "";

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//Funciones
function leerInputs() {
    const numCodigo = document.getElementById('txtCodigo').value;
    const categoria = document.getElementById('txtCategoria').value;
    const nombreL = document.getElementById('txtNombreL').value;
    const precio = document.getElementById('txtPrecio').value;
    const urlImag = document.getElementById('txtUrl').value;

    return { numCodigo, categoria, nombreL, precio, urlImag };

}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la base de datos
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    alert("ingrese a add db");
    const { numCodigo, categoria, nombreL, precio, urlImag } = leerInputs();
    //validar
    if (numCodigo === "" || categoria === "" || nombreL === "" || precio === "") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    set(
        refS(db, 'Productos/' + numCodigo),
        {

            numCodigo: numCodigo,
            categoria: categoria,
            nombreL: nombreL,
            precio: precio,
            urlImag: urlImag
        }
    ).then(() => {

        alert("Se agrego con exito");
        Listarproductos();
        limpiarInputs();
    }).catch((error) => {
        alert("Ocurrio un error");
    });
}

function limpiarInputs() {
    document.getElementById('txtCodigo').value = '';
    document.getElementById('txtCategoria').value = '';
    document.getElementById('txtNombreL').value = '';
    document.getElementById('txtPrecio').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs(numCodigo, categoria, nombreL, precio, urlImag) {
    document.getElementById('txtCodigo').value = numCodigo;
    document.getElementById('txtCategoria').value = categoria;
    document.getElementById('txtNombreL').value = nombreL;
    document.getElementById('txtPrecio').value = precio;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    const numCodigo = document.getElementById('txtCodigo').value.trim();
    if (numCodigo === "") {
        mostrarMensaje("No se ingreso un num de Codigo");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + numCodigo)).then((snapshot) => {
        if (snapshot.exists()) {
            const { categoria, nombreL, precio, urlImag } = snapshot.val();
            escribirInputs(numCodigo, categoria, nombreL, precio, urlImag);
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + numCodigo + " No Existe");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Productos');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.categoria;
            fila.appendChild(celdaNombre);


            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.nombreL;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.precio;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

Listarproductos();

//Funcion actualizar

function actualizarProducto() {
    alert("Ingrese a add db");
    const { numCodigo, categoria, nombreL, precio, urlImag } = leerInputs();
    if (numCodigo === "" || categoria === "" || nombreL === "" || precio === "") {
        mostrarMensaje("Faltaron datos por capturar");
        return;
    }
    alert("Se actualizo con exito");
    update(refS(db, 'Productos/' + numCodigo), {
        numCodigo: numCodigo,
        categoria: categoria,
        nombreL: nombreL,
        precio: precio,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();

    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
    Listarproductos();
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);

//Funcion Borrar

function eliminarProducto() {
    const numCodigo = document.getElementById('txtCodigo').value.trim();
    if (numCodigo === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + numCodigo)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Productos/' + numCodigo)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                Listarproductos();
                limpiarInputs();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con ID " + numCodigo + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';

                }, 500);
            }).catch((error) => {
                console.error(error);
            });
        });

    }
});
